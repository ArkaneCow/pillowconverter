#include "voxel.h"

voxel::voxel()
{
    vector3 default_pos = vector3();
    vector3 default_normal = vector3();
    color default_color = color();
    this->v_position = default_pos;
    this->v_normal = default_normal;
    this->v_color = default_color;
}

voxel::voxel(vector3 pos, vector3 norm, color col) {
    this->v_position = pos;
    this->v_normal = norm;
    this->v_color = col;
}

vector3 voxel::get_position() {
    return this->v_position;
}

vector3 voxel::get_normal() {
    return this->v_normal;
}

color voxel::get_color() {
    return this->v_color;
}

void voxel::set_position(vector3 v) {
    this->v_position = v;
}

void voxel::set_normal(vector3 v) {
    this->v_normal = v;
}

void voxel::set_color(color c) {
    this->v_color = c;
}
