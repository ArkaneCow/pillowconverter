#ifndef EDGE3_H
#define EDGE3_H

#include <vector>

#include "voxel.h"
#include "color.h"

using namespace std;

class edge3
{
public:
    edge3();
    edge3(voxel a, voxel b, int divisions);
    vector<voxel> voxels;
    float get_length();
private:
    voxel a;
    voxel b;
    int divisions;

    void interpolate();
};

#endif // EDGE3_H
