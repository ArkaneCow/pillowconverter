#ifndef VECTOR3_H
#define VECTOR3_H

#include <math.h>

class vector3
{
public:
    vector3();
    vector3(float x, float y, float z);
    void set_x(float f);
    void set_y(float f);
    void set_z(float f);
    float get_x();
    float get_y();
    float get_z();
    float magnitude();
    float distance_from(vector3 v);
    void normalize();
    void negative();
    float dot_product(vector3 v);
    void cross_product(vector3 v);
    float angle_between(vector3 v);
    void add(float f);
    void add(vector3 v);
    void subtract(float f);
    void subtract(vector3 v);
    void multiply(float f);
    void multiply(vector3 v);
    void divide(float f);
    void divide(vector3 v);
    virtual ~vector3();
private:
    float x;
    float y;
    float z;
};

#endif // VECTOR3_H
