/*
 * mesh.h
 *
 *  Created on: Apr 10, 2014
 *      Author: jwpilly
 */

#ifndef MESH_H_
#define MESH_H_

#include <vector>

#include "face.h"
#include "putils.h"

using namespace std;

class mesh {
public:
    vector<face> faces;
    void from_obj(const char* filename);
	mesh();
    virtual ~mesh();
};

#endif /* MESH_H_ */
