#include <iostream>
#include <string.h>

#include <stdlib.h>

#include "converter.h"

using namespace std;

bool check_extension(string filename, string extension) {
    string file_extension = filename;
    char delimiter = '.';
    int d_index = -1;
    for (unsigned int i = 0 ; i < filename.length(); i++) {
        if (filename[i] == delimiter) {
            d_index = i;
        }
    }
    if (d_index > -1) {
        file_extension = filename.substr(d_index + 1);
    }
    if (strcmp(file_extension.c_str(), extension.c_str()) == 0) {
        return true;
    }
    return false;
}

int main(int argc, char* argv[])
{
    cout << "Hello World!" << endl;
    string param_list = "";
    for (int i = 0; i < argc; i++) {
        param_list += string(argv[i]) + " ";
    }
    cout << "Parameters: " << param_list << endl;
    string mesh_file;
    string texture_file;
    int divisions = 0;
    const string m_prefix = "--mesh=";
    const string t_prefix = "--texture=";
    const string d_prefix = "--divisions=";
    const string m_ext = "obj";
    const string t_ext = "ppm";
    for (int i = 0; i < argc; i++) {
        string parameter(argv[i]);
        if (strcmp(parameter.substr(0, m_prefix.length()).c_str(), m_prefix.c_str()) == 0) {
            mesh_file = parameter.substr(m_prefix.length());
            cout << "mesh file: " << mesh_file << endl;
            if (!check_extension(mesh_file, m_ext)) {
                cout << "Unsupported mesh file extension!" << endl;
                exit(0);
            }
        } else if (strcmp(parameter.substr(0, t_prefix.length()).c_str(), t_prefix.c_str()) == 0) {
            texture_file = parameter.substr(t_prefix.length());
            cout << "texture file: " << texture_file << endl;
            if (!check_extension(texture_file, t_ext)) {
                cout << "Unsupported texture file extension!" << endl;
                exit(0);
            }
        } else if (strcmp(parameter.substr(0, d_prefix.length()).c_str(), d_prefix.c_str()) == 0) {
            divisions = atoi(parameter.substr(d_prefix.length()).c_str());
            cout << "divisions: " << divisions << endl;
        }
    }
    if (mesh_file.empty()) {
        cout << "Required parameter missing - mesh file" << endl;
        exit(0);
    }
    string output_file = mesh_file + ".bad";
    if (texture_file.empty()) {
        cout << "Converting without texture file" << endl;
        converter m_convert = converter(mesh_file.c_str());
        if (divisions > 0) {
            m_convert.convert(divisions);
        } else {
            m_convert.convert();
        }
        m_convert.write(output_file.c_str());
    } else {
        cout << "Converting with texture file" << endl;
        converter m_convert = converter(mesh_file.c_str(), texture_file.c_str());
        if (divisions > 0) {
            m_convert.convert(divisions);
        } else {
            m_convert.convert();
        }
        m_convert.write(output_file.c_str());
    }
    cout << "Conversion completed" << endl;
    return 0;
}

