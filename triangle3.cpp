#include "triangle3.h"

triangle3::triangle3() {
	vector3* a = new vector3();
	vector3* b = new vector3();
	vector3* c = new vector3();
	this->vertices[0] = *a;
	this->vertices[1] = *b;
	this->vertices[2] = *c;
}

triangle3::triangle3(vector3 a, vector3 b, vector3 c) {
    this->vertices[0] = a;
    this->vertices[1] = b;
    this->vertices[2] = c;
	calculate_normal();
}

triangle3::triangle3(vector3* vertices) {
	this->vertices[0] = vertices[0];
	this->vertices[1] = vertices[1];
	this->vertices[2] = vertices[2];
	calculate_normal();
}

vector3 triangle3::get_normal() {
    return this->normal;
}

void triangle3::calculate_normal() {
    vector3 u = this->vertices[1];
    u.subtract(this->vertices[0]);
    vector3 v = this->vertices[2];
    v.subtract(this->vertices[0]);
    u.cross_product(v);
    u.normalize();
    this->normal = u;
}

vector3* triangle3::get_vertices() {
	return &this->vertices[0];
}

vector3 triangle3::get_center() {
	float x = (vertices[0].get_x() + vertices[1].get_x() + vertices[2].get_x()) / 3;
	float y = (vertices[0].get_y() + vertices[1].get_y() + vertices[2].get_y()) / 3;
	float z = (vertices[0].get_z() + vertices[1].get_z() + vertices[2].get_z()) / 3;
    vector3 center = vector3(x, y, z);
	return center;
}

triangle3::~triangle3() {

}
