#ifndef VOXEL_H
#define VOXEL_H

#include "vector3.h"
#include "color.h"

class voxel
{
public:
    voxel();
    voxel(vector3 pos, vector3 norm, color col);
    vector3 get_position();
    vector3 get_normal();
    color get_color();
    void set_position(vector3 v);
    void set_normal(vector3 v);
    void set_color(color c);
private:
    vector3 v_position;
    vector3 v_normal;
    color v_color;
};

#endif // VOXEL_H
