#include "converter.h"

converter::converter(const char* mesh_file)
{
    cout << "Initializing converter with mesh file: " << mesh_file << endl;
    this->model.from_obj(mesh_file);
    //create fake sold-color texture
    this->texture = image(100, 100);
}

converter::converter(const char* mesh_file, const char* texture_file) {
    cout << "Initializing converter with mesh file: " << mesh_file << " and texture file: " << texture_file << endl;
    this->model.from_obj(mesh_file);
    this->texture.from_ppm_raw(texture_file);
}

void converter::convert() {
    //this is highly inefficient
    cout << "Converting with no divisions" << endl;
    cout << "Adding vertices to vector" << endl;
    for (unsigned int f = 0; f < this->model.faces.size(); f++) {
        triangle3 tri = this->model.faces[f].get_triangle();
        vector3* vertices = tri.get_vertices();
        for (unsigned int v = 0; v < 3; v++) {
            //if no voxels yet, or vector doesn't contain voxel, then add voxel
            if (this->voxels.empty()) {
                voxel vox = voxel();
                vox.set_position(vertices[v]);
                this->voxels.push_back(vox);
            } else {
                bool contains = false;
                for (unsigned int e = 0; e < this->voxels.size(); e++) {
                    vector3 e_pos = this->voxels[e].get_position();
                    if (e_pos.get_x() == vertices[v].get_x() && e_pos.get_y() == vertices[v].get_y() && e_pos.get_z() == vertices[v].get_z()) {
                        contains = true;
                    }
                }
                if (!contains) {
                    voxel vox = voxel();
                    vox.set_position(vertices[v]);
                    this->voxels.push_back(vox);
                }
            }
        }
    }
    cout << "Calculating vertex normals" << endl;
    for (unsigned int f = 0; f < this->model.faces.size(); f++) {
        triangle3 tri = this->model.faces[f].get_triangle();
        vector3* vertices = tri.get_vertices();
        vector3 face_normal = this->model.faces[f].get_triangle().get_normal();
        for (unsigned int v = 0; v < 3; v++) {
            vector3 f_vertex = vertices[v];
            for (unsigned int e = 0; e < this->voxels.size(); e++) {
                vector3 e_pos = this->voxels[e].get_position();
                if (e_pos.get_x() == f_vertex.get_x() && e_pos.get_y() == f_vertex.get_y() && e_pos.get_z() == f_vertex.get_z()) {
                    vector3 v_normal = this->voxels[e].get_normal();
                    v_normal.add(face_normal);
                    v_normal.normalize();
                    this->voxels[e].set_normal(v_normal);
                }
            }
        }
    }
    cout << "Setting voxel colors based on UVs" << endl;
    for (unsigned int f = 0; f < this->model.faces.size(); f++) {
        triangle3 tri = this->model.faces[f].get_triangle();
        vector3* vertices = tri.get_vertices();
        for (unsigned int v = 0; v < 3; v++) {
            vector3 f_vertex = vertices[v];
            for (unsigned int e = 0; e < voxels.size(); e++) {
                vector3 e_pos = this->voxels[e].get_position();
                if (e_pos.get_x() == f_vertex.get_x() && e_pos.get_y() == f_vertex.get_y() && e_pos.get_z() == f_vertex.get_z()) {
                    vector2 v_uv = this->model.faces[f].get_uvs()[v];
                    float t_x = v_uv.get_x() * this->texture.get_width();
                    float t_y = v_uv.get_y() * this->texture.get_height();
                    color v_color = this->texture.get_color((int) t_x, (int) t_y);
                    this->voxels[e].set_color(v_color);
                }
            }
        }
    }
}

void converter::convert(int divisions) {
    cout << "Converting with " << divisions << " divisions" << endl;
    cout << "Adding vertices to vector" << endl;
    for (unsigned int f = 0; f < this->model.faces.size(); f++) {
        triangle3 tri = this->model.faces[f].get_triangle();
        vector3* vertices = tri.get_vertices();
        for (unsigned int v = 0; v < 3; v++) {
            //if no voxels yet, or vector doesn't contain voxel, then add voxel
            if (this->voxels.empty()) {
                voxel vox = voxel();
                vox.set_position(vertices[v]);
                this->voxels.push_back(vox);
            } else {
                bool contains = false;
                for (unsigned int e = 0; e < this->voxels.size(); e++) {
                    vector3 e_pos = this->voxels[e].get_position();
                    if (e_pos.get_x() == vertices[v].get_x() && e_pos.get_y() == vertices[v].get_y() && e_pos.get_z() == vertices[v].get_z()) {
                        contains = true;
                    }
                }
                if (!contains) {
                    voxel vox = voxel();
                    vox.set_position(vertices[v]);
                    this->voxels.push_back(vox);
                }
            }
        }
    }
    cout << "Calculating vertex normals" << endl;
    for (unsigned int f = 0; f < this->model.faces.size(); f++) {
        triangle3 tri = this->model.faces[f].get_triangle();
        vector3* vertices = tri.get_vertices();
        vector3 face_normal = this->model.faces[f].get_triangle().get_normal();
        for (unsigned int v = 0; v < 3; v++) {
            vector3 f_vertex = vertices[v];
            for (unsigned int e = 0; e < this->voxels.size(); e++) {
                vector3 e_pos = this->voxels[e].get_position();
                if (e_pos.get_x() == f_vertex.get_x() && e_pos.get_y() == f_vertex.get_y() && e_pos.get_z() == f_vertex.get_z()) {
                    vector3 v_normal = this->voxels[e].get_normal();
                    v_normal.add(face_normal);
                    v_normal.normalize();
                    this->voxels[e].set_normal(v_normal);
                }
            }
        }
    }
    cout << "Setting voxel colors based on UVs" << endl;
    for (unsigned int f = 0; f < this->model.faces.size(); f++) {
        triangle3 tri = this->model.faces[f].get_triangle();
        vector3* vertices = tri.get_vertices();
        for (unsigned int v = 0; v < 3; v++) {
            vector3 f_vertex = vertices[v];
            for (unsigned int e = 0; e < voxels.size(); e++) {
                vector3 e_pos = this->voxels[e].get_position();
                if (e_pos.get_x() == f_vertex.get_x() && e_pos.get_y() == f_vertex.get_y() && e_pos.get_z() == f_vertex.get_z()) {
                    vector2 v_uv = this->model.faces[f].get_uvs()[v];
                    float t_x = v_uv.get_x() * this->texture.get_width();
                    float t_y = v_uv.get_y() * this->texture.get_height();
                    color v_color = this->texture.get_color((int) t_x, (int) t_y);
                    this->voxels[e].set_color(v_color);
                }
            }
        }
    }
    cout << "Generating intermediate voxels using interpolation" << endl;
    for (unsigned int f = 0; f < this->model.faces.size(); f++) {
        voxel original[3];
        vector3 verts[3];
        verts[0] = this->model.faces[f].get_triangle().get_vertices()[0];
        verts[1] = this->model.faces[f].get_triangle().get_vertices()[1];
        verts[2] = this->model.faces[f].get_triangle().get_vertices()[2];
        for (unsigned int v = 0; v < this->voxels.size(); v++) {
            voxel comp = this->voxels[v];
            if (comp.get_position().get_x() == verts[0].get_x() && comp.get_position().get_y() == verts[0].get_y() && comp.get_position().get_z() == verts[0].get_z()) {
                original[0] = comp;
            } else if (comp.get_position().get_x() == verts[1].get_x() && comp.get_position().get_y() == verts[1].get_y() && comp.get_position().get_z() == verts[1].get_z()) {
                original[1] = comp;
            } else if (comp.get_position().get_x() == verts[2].get_x() && comp.get_position().get_y() == verts[2].get_y() && comp.get_position().get_z() == verts[2].get_z()) {
                original[2] = comp;
            }
        }
        edge3 edges[3];
        edges[0] = edge3(original[0], original[1], divisions);
        edges[1] = edge3(original[1], original[2], divisions);
        edges[2] = edge3(original[2], original[0], divisions);
        for (unsigned int e = 0; e < 3; e++) {
            //exclude voxels a and b
            for (int v = 1; v < divisions; v++) {
                this->voxels.push_back(edges[e].voxels[v]);
            }
        }
        vector<edge3> gen_edge;
        float length = 0;
        int longest = 0;
        for (unsigned int i = 0; i < 3; i++) {
            float edge_length = edges[i].get_length();
            if (edge_length > length) {
                length = edge_length;
                longest = i;
            }
        }
        int short_1 = (longest + 1) % 3;
        int short_2 = (longest + 2) % 3;
        for (int a = 1; a < divisions; a++) {
            edge3 e_a = edge3(edges[longest].voxels[a], edges[short_1].voxels[a], divisions);
            gen_edge.push_back(e_a);
        }
        for (int b = 1; b < divisions; b++) {
            edge3 e_b = edge3(edges[longest].voxels[b], edges[short_2].voxels[b], divisions);
            gen_edge.push_back(e_b);
        }
        for (unsigned int g = 0; g < gen_edge.size(); g++) {
            for (int v = 1; v < divisions; v++) {
                this->voxels.push_back(gen_edge[g].voxels[v]);
            }
        }
    }
}

void converter::write(const char* filename) {
    string file_data = "";
    for (unsigned int i = 0; i < this->voxels.size(); i++) {
        voxel v = this->voxels[i];
        //any way to reuse stringstreams???
        stringstream px;
        stringstream py;
        stringstream pz;
        stringstream nx;
        stringstream ny;
        stringstream nz;
        int r = (int) v.get_color().get_red();
        int g = (int) v.get_color().get_green();
        int b = (int) v.get_color().get_blue();
        int a = (int) v.get_color().get_alpha();
        stringstream cr;
        stringstream cg;
        stringstream cb;
        stringstream ca;
        px << v.get_position().get_x();
        py << v.get_position().get_y();
        pz << v.get_position().get_z();
        nx << v.get_normal().get_x();
        ny << v.get_normal().get_y();
        nz << v.get_normal().get_z();
        cr << r;
        cg << g;
        cb << b;
        ca << a;
        file_data += "[" + px.str() + "," + py.str() + "," + pz.str() + "]";
        file_data += "/[" + nx.str() + "," + ny.str() + "," + nz.str() + "]";
        file_data += "/[" + cr.str() + "," + cg.str() + "," + cb.str() + "," + ca.str() + "]\n";
    }
    cout << "Finished constructing file data" << endl;
    ofstream output_file;
    output_file.open(filename);
    if (output_file.is_open()) {
        output_file << file_data;
        output_file.close();
        cout << "Finished writing file data" << endl;
    } else {
        cout << "Error opening file: " << filename << " for writing" << endl;
        exit(1);
    }
}

converter::~converter() {

}
