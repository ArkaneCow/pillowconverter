#ifndef CONVERTER_H
#define CONVERTER_H

#include <string.h>
#include <vector>
#include <fstream>

#include "voxel.h"
#include "mesh.h"
#include "face.h"
#include "image.h"
#include "edge3.h"

class converter
{
public:
    converter(const char* mesh_file);
    converter(const char* mesh_file, const char* texture_file);
    void convert();
    void convert(int divisions);
    void write(const char* filename);
    virtual ~converter();
private:
    mesh model;
    image texture;
    vector<voxel> voxels;
};

#endif // CONVERTER_H
