#include "edge3.h"

edge3::edge3()
{
}

edge3::edge3(voxel a, voxel b, int divisions) {
    this->a = a;
    this->b = b;
    this->divisions = divisions;
    interpolate();
}

void edge3::interpolate() {
    vector3 p_delta = b.get_position();
    p_delta.subtract(a.get_position());
    vector3 n_delta = b.get_normal();
    n_delta.subtract(a.get_normal());
    color c_delta = b.get_color();
    c_delta.subtract(a.get_color());
    vector3 p_step = p_delta;
    p_step.divide(this->divisions);
    vector3 n_step = n_delta;
    n_step.divide(this->divisions);
    color c_step = c_delta;
    c_step.divide(this->divisions);
    for (int i = 0; i <= this->divisions; i++) {
        vector3 v_pos = a.get_position();
        vector3 v_norm = a.get_normal();
        color v_col = a.get_color();
        vector3 pos_add = p_step;
        pos_add.multiply((float) i);
        vector3 norm_add = n_step;
        norm_add.multiply((float) i);
        color col_add = c_step;
        col_add.multiply((float) i);
        v_pos.add(pos_add);
        v_norm.add(norm_add);
        v_col.add(col_add);
        voxel v = voxel(v_pos, v_norm, v_col);
        this->voxels.push_back(v);
    }
}

float edge3::get_length() {
    vector3 p_delta = b.get_position();
    p_delta.subtract(a.get_position());
    return p_delta.magnitude();
}
