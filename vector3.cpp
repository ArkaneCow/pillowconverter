#include "vector3.h"

vector3::vector3()
{
    this->x = 0;
    this->y = 0;
    this->z = 0;
}

vector3::vector3(float x, float y, float z) {
    this->x = x;
    this->y = y;
    this->z = z;
}

void vector3::set_x(float f) {
    this->x = f;
}

void vector3::set_y(float f) {
    this->y = f;
}

void vector3::set_z(float f) {
    this->z = f;
}

float vector3::get_x() {
    return this->x;
}

float vector3::get_y() {
    return this->y;
}

float vector3::get_z() {
    return this->z;
}

float vector3::magnitude() {
    return sqrt((this->x * this->x) + (this->y * this->y) + (this->z * this->z));
}

float vector3::distance_from(vector3 v) {
    float x_2 = v.get_x();
    float y_2 = v.get_y();
    float z_2 = v.get_z();
    float distance = sqrt(((x_2 - v.get_x()) * (x_2 - v.get_x())) + ((y_2 - v.get_y()) * (y_2 - v.get_y())) + ((z_2 - v.get_z()) * (z_2 - v.get_z())));
    return distance;
}

void vector3::normalize() {
    float magnitude = this->magnitude();
    this->x /= magnitude;
    this->y /= magnitude;
    this->z /= magnitude;
}

void vector3::negative() {
    this->x = -this->x;
    this->y = -this->y;
    this->z = -this->z;
}

float vector3::dot_product(vector3 v) {
    return (this->x * v.get_x()) + (this->y * v.get_y()) + (this->z * v.get_z());
}

void vector3::cross_product(vector3 v) {
    float x = this->x;
    float y = this->y;
    float z = this->z;
    this->x = y * v.get_z() - z * v.get_y();
    this->y = z * v.get_x() - x * v.get_z();
    this->z = x * v.get_y() - y * v.get_x();
}

float vector3::angle_between(vector3 v) {
    float total_magnitude = this->magnitude() + v.magnitude();
    float dot_product = this->dot_product(v);
    float angle_radian = acos(dot_product / total_magnitude);
    float angle = ((float) 180 * angle_radian) / (float) 3.14;
    return angle;
}

void vector3::add(float f) {
    this->x += f;
    this->y += f;
    this->z += f;
}

void vector3::add(vector3 v) {
    this->x += v.get_x();
    this->y += v.get_y();
    this->z += v.get_z();
}

void vector3::subtract(float f) {
    this->x -= f;
    this->y -= f;
    this->z -= f;
}

void vector3::subtract(vector3 v) {
    this->x -= v.get_x();
    this->y -= v.get_y();
    this->z -= v.get_z();
}

void vector3::multiply(float f) {
    this->x *= f;
    this->y *= f;
    this->z *= f;
}

void vector3::multiply(vector3 v) {
    this->x *= v.get_x();
    this->y *= v.get_y();
    this->z *= v.get_z();
}

void vector3::divide(float f) {
    this->x /= f;
    this->y /= f;
    this->z /= f;
}

void vector3::divide(vector3 v) {
    this->x /= v.get_x();
    this->y /= v.get_y();
    this->z /= v.get_z();
}

vector3::~vector3() {

}
