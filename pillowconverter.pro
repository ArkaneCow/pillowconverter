TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    converter.cpp \
    vector3.cpp \
    voxel.cpp \
    color.cpp \
    image.cpp \
    face.cpp \
    mesh.cpp \
    triangle3.cpp \
    vector2.cpp \
    putils.cpp \
    edge3.cpp

HEADERS += \
    converter.h \
    vector3.h \
    voxel.h \
    color.h \
    image.h \
    face.h \
    mesh.h \
    triangle3.h \
    vector2.h \
    putils.h \
    edge3.h

